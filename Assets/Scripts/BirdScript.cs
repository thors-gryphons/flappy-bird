using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdScript : MonoBehaviour
{
    Rigidbody2D rb2d;
    [SerializeField]
    private float flapForce = 100f;
    private bool isDead;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        //Time.timeScale = 0;
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
     /*   public void unFreeze()
    {
        Time.timeScale = 1;
    }*/
    // Update is called once per frame
    void Update()
    {
            if ((Input.GetMouseButton(0) || Input.GetKeyDown(KeyCode.Space) && !isDead))
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(0, flapForce));
            }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isDead = true;
        rb2d.velocity = Vector2.zero;
        GetComponent<Animator>().SetBool("isDead", true);
        GameManager.instance.BirdDied();
    }

}
